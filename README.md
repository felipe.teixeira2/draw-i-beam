# DRAW I-BEAM CROSS-SECTION

Given a profile name:
1. Finds profile in library
2. Collects its geometric properties
3. Draws the I-shape on the screen
